-- Martensen, Ian Kenneth A.
-- Perform CRUD operations on the previously created database.

-- Select/Use a database;
-- Syntax: USE database_name
USE blog_db;

-- Create/Add a tables.
-- Syntax: CREATE TABLE table_name(column1, column2, PRIMARY KEY(id));
CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(100) NOT NULL,
password VARCHAR(300) NOT NULL,
datetime_created DATETIME NOT NULL,
PRIMARY KEY(id)
);

-- Create/Add a tables.
-- Syntax: CREATE TABLE table_name(column1, column2, PRIMARY KEY(id));
CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
user_id INT NOT NULL,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_posted DATETIME NOT NULL,
PRIMARY KEY(id)
);

-- Adding a record with mutiple columns.
-- Users
INSERT INTO users(email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users(email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users(email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
INSERT INTO users(email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users(email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

-- Adding a record with mutiple columns.
-- Posts
INSERT INTO posts(user_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00");
INSERT INTO posts(user_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00");
INSERT INTO posts(user_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-03 03:00:00");
INSERT INTO posts(user_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

-- 4. Get all the post with an author ID of 1.
SELECT * FROM posts WHERE user_id = 1;
-- Answer: 
-- +----+---------+-------------+--------------+---------------------+
-- | id | user_id | title       | content      | datetime_posted     |
-- +----+---------+-------------+--------------+---------------------+
-- |  1 |       1 | First Code  | Hello World! | 2021-01-02 01:00:00 |
-- |  2 |       1 | Second Code | Hello Earth! | 2021-01-02 02:00:00 |
-- +----+---------+-------------+--------------+---------------------+

-- 5. Get all the user's email and datetime of creation.
SELECT email, datetime_created FROM users;
-- Answer: 
-- +-------------------------+---------------------+
-- | email                   | datetime_created    |
-- +-------------------------+---------------------+
-- | johnsmith@gmail.com     | 2021-01-01 01:00:00 |
-- | juandelacruz@gmail.com  | 2021-01-01 02:00:00 |
-- | janesmith@gmail.com     | 2021-01-01 03:00:00 |
-- | mariadelacruz@gmail.com | 2021-01-01 04:00:00 |
-- | johndoe@gmail.com       | 2021-01-01 05:00:00 |
-- +-------------------------+---------------------+

-- 6. Update a post's content to Hello to the people of the Earth! where its initial content is Hello Earth! by using record's ID.
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;
-- Answer:
-- +----+---------+-------------+-----------------------------------+---------------------+
-- | id | user_id | title       | content                           | datetime_posted     |
-- +----+---------+-------------+-----------------------------------+---------------------+
-- |  1 |       1 | First Code  | Hello World!                      | 2021-01-02 01:00:00 |
-- |  2 |       1 | Second Code | Hello to the people of the Earth! | 2021-01-02 02:00:00 |
-- |  3 |       2 | Third Code  | Welcome to Mars!                  | 2021-01-03 03:00:00 |
-- |  4 |       4 | Fourth Code | Bye bye solar system!             | 2021-01-02 04:00:00 |
-- +----+---------+-------------+-----------------------------------+---------------------+

-- 7. Delete the user with an email of johndoe@gmail.com.
DELETE FROM users WHERE email = "johndoe@gmail.com";
-- Answer:
-- +----+-------------------------+-----------+---------------------+
-- | id | email                   | password  | datetime_created    |
-- +----+-------------------------+-----------+---------------------+
-- |  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
-- |  2 | juandelacruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
-- |  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
-- |  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
-- +----+-------------------------+-----------+---------------------+

